// Теоретический вопрос
// Обьясните своими словами, как вы понимаете асинхронность в Javascript
//
// Задание
// Написать программу "Я тебя по айпи вычислю"
//
// Технические требования:
//
//     Создать простую HTML страницу с кнопкой Вычислить по IP.
//     По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
//     Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
//     Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
//     Все запросы на сервер необходимо выполнить с помощью async await.
//
//
// Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
//
// теория асинхронность это последовательность выполнениия задачи которая идет не по порядку а по мере выполнения

//праактиика


class Information {
    constructor({city, continent, country, district, regionName}) {
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
    }
}

function createInformationHtml({city, continent, country, district, regionName}) {
    return `<div><p>${city}</p> <p>${continent}</p> <p>${country}</p> <p>${district}</p> <p>${regionName}</p></div>`;
}

const button = document.getElementById('IP');

async function getIp() {
    const response = await fetch(`https://api.ipify.org/?format=json`);
    const {ip} = await response.json();

    return ip;
};

async function getAddressByIp(ip) {
    const endpoint = 'http://ip-api.com/batch';
    const data = [
        {"query": ip, "fields": "continent,country,regionName,city,district", "lang": "ru"},
    ];

    const response = await fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(data)
    });
    const [address] = await response.json();

    return address;
};


button.addEventListener('click', async (e) => {
    e.preventDefault();
    const ip = await getIp();
    const address = await getAddressByIp(ip);
    const info = new Information(address);

    document.getElementById('information').innerHTML = createInformationHtml(info);
});


