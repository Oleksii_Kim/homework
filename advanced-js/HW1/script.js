// Теоретический вопрос
//
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//
// Задание
//
// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
// Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Создайте геттеры и сеттеры для этих свойств.
//     Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.


// Теория - прототипное наследование это наcледование обьекта если например данные однонго обьекта мы хотим добавитть в другой обьект .
// задание

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        if (value.length >= 3)  {
            this._name = value;
        }
    }
    get age() {
        return this._age;
    }
    set age(val) {
        if (val.numberValue >= 18) {
            this._age = val;
        }
    }
    get salary() {
        return this._salary;
    }
    set salary(val) {
        if (val >= 1000) {
            this._salary = val;
        }
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name,age,salary);
        this._lang = lang
    }

    get salary() {
        return this._salary;
    }

    set salary(val) {
        if (val >= 1000) {
            this._salary = val*3;
        }
    }
}

const Alex = new Programmer('Alex','28','1200','english')
const Dima = new Programmer('Dima','20','1000','russian')
const Pasha = new Programmer('Pasha','23','1500','ukrainian')
console.log(Alex);
console.log(Dima);
console.log(Pasha);