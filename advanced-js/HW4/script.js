// Теоретический вопрос
// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
//
//     Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.
//
//     Технические требования:
//
//     Отправить AJAX запрос по адресу https://ajax.test-danit.com/api/swapi/films и получить список всех фильмов серии Звездные войны
//
//     Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
//     Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма,
//     а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
//
//


// Теория Ajax асинхронные запросы на сервер, полезен при разработке когда нам не надо перезагружать страницу всю страанницу аа просто что б данные подгружались , для работы с серверами


//практика


fetch('https://ajax.test-danit.com/api/swapi/films ')
    .then(response => response.json())
    .then(films => {
        const filmsHTML = films.map(({name, episodeId, openingCrawl, characters}) => {
            return Promise.all(characters.map((url) => fetch(url)
                .then((response) => response.json())
                .then(character => `<li>${character.name}</li>`)
            ))
                .then(characterListItem => characterListItem.reduce((list, current) => list + current, ''
                ))
                .then(characterHTMLItem => {
                    const list = `<ul>${characterHTMLItem}</ul>`;
                    return `<li><p>${episodeId}</p><h>${name}</h><p>${openingCrawl}</p> ${list}</li>`;
                });
        });
        Promise.all(filmsHTML)
            .then(arrayOfFilms => arrayOfFilms.reduce((list, current) => list + current, ''))
            .then(listItem => `<ul>${listItem}</ul>`)
            .then(list => document.getElementById('app').innerHTML = list);
    });


// const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films';
//
// function getFilms() {
//     return fetch(filmsUrl).then((res) => res.json());
// }
//
// function parseFilmsList(films) {
//     const listItems = films.reduce((items, film) => {
//         items = items + filmToHtml(film);
//
//         return items;
//     }, '');
//
//     return `<ul>${listItems}</ul>`;
// }
//
// function loadCharacters(urls) {
//     const characterPromises = urls.map((url) =>
//         fetch(url).then((res) => res.json())
//     );
//
//     return Promise.all(characterPromises);
// }
//
// function parseCharacters(characters) {
//     return characters.reduce((list, character) => {
//         list = list + characterToHtml(character);
//
//         return list;
//     }, '<h3>Characters:</h3>');
// }
//
// function characterToHtml({ name }) {
//     return `<li>${name}</li>`;
// }
//
// function filmToHtml({ name, director, openingCrawl, id }) {
//     const charactersContainerId = `film-${id}-characters`;
//
//     const filmHtml = `
//   <li>
//     <h2>${name}</h2>
//     <p>${openingCrawl}</p>
//     <div><i><b>Director: ${director}</i></b></div>
//     <ul id="${charactersContainerId}">
//     loading...
//     </ul>
//   </li>
// `;
//     return filmHtml;
// }
//
// // Write Javascript code!
// const appDiv = document.getElementById('app');
//
// getFilms()
//     .then((films) => {
//         appDiv.innerHTML = parseFilmsList(films);
//
//         return films;
//     })
//     .then((films) => {
//         films.forEach((film, i) => {
//             const charactersContainerId = `film-${film.id}-characters`;
//             const container = document.getElementById(charactersContainerId);
//             const characterUrls = film.characters;
//
//             loadCharacters(characterUrls).then((characters) => {
//                 container.innerHTML = parseCharacters(characters);
//             });
//         });
//     });
