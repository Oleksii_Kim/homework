// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price).
// Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
//     Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

// теория уместно использовать эту конструкцию при проверке ошибок . он ищет ошибки и использует другой код для роботы  с этой ошибкой
// напимер есть масив с обьектвмии каак на задании и нужно выделить обьекты тоьлко с определенными параметами при этом ошибка прекращаает дальнейшее выполнения функцуии тогда уместно использовать тут эту конструкцию7


// практика


let rootDiv = document.getElementById('root');

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function checkObject(item) {
    if (item.author === undefined) {
        throw Error('Автор не указан в обьекте');
    }
    if (item.name === undefined) {
        throw Error('Имя не указано в обьекте');
    }
    if (item.price === undefined) {
        throw Error('цена не указана в обьекте');
    }
    return item;
}


function createList() {
    let listItem = document.createElement('ul');
    rootDiv.append(listItem);
    books.forEach(element => {
        try {
            const validBook = checkObject(element);
            let li = document.createElement('li');
            li.innerHTML = (` ${validBook.author}, ${validBook.name} , ${validBook.price}`);
            listItem.append(li);
        } catch (error) {
            console.log(error);
        }
    });
}

createList();
