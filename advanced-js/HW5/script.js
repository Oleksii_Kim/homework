// Задание
//
// Создать страницу, имитирующую ленту новостей социальной сети Twitter.
//
//
//     Технические требования:
//
//     При открытии страницы, необходимо получить с сервера список всех пользователей и общий список публикаций.
//     Для этого нужно отправить GET запрос на следующие два адреса:
//
//     https://ajax.test-danit.com/api/json/users
//         https://ajax.test-danit.com/api/json/posts
//
//
//             После загрузки всех пользователей и их публикаций, необоходимо отобразить все публикации на странице.
//     Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), и включать заголовок, текст,
//      а также имя, фамилию и имейл пользователя, который ее разместил.
//     На каждой карточке должна присутствовать иконка или кнопка, которая позволит удалить данную карточку со страницы.
//     При клике на нее необходимо отправить DELETE запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}.
//     После получения подтверждения с сервера (запрос прошел успешно), карточку можно удалить со страницы, используя JavaScript.
//     Более детальную информацию по использованию каждого из этих указанных выше API можно найти здесь.
//     Данный сервер является тестовым. После перезагрузки страницы все изменения, которые отправлялись на сервер, не будут там сохранены. Это нормально, все так и должно работать.
//     Карточки обязательно должны быть реализованы в виде ES6 классов. Для этого необходимо создать класс Card. При необходимости, вы можете добавлять также другие классы.
//
//
//     Необязательное задание продвинутой сложности
//
// Пока с сервера при открытии страницы загружается информация, показывать анимацию загрузки. Анимацию можно использовать любую.
//     Желательно найти вариант на чистом CSS без использования JavaScript.
//     Добавить вверху страницы кнопку Добавить публикацию. При нажатии на кнопку, открывать модальное окно, в котором пользователь сможет ввести заголовок и текст публикации.
//     После создания публикации данные о ней необходимо отправить в POST запросе по адресу:
//     https://ajax.test-danit.com/api/json/posts. Новая публикация должна быть добавлена вверху страницы (сортировка в обратном хронологическом порядке).
//         // В качестве автора можно присвоить публикации пользователя с id: 1.
//     Добавить функционал (иконку) для редактирования содержимого карточки.
//     После редактирования карточки для подтверждения изменений необходимо отправить PUT запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}.


class Card {
    constructor(title, body, name, email, postId) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.postId = postId;
    }
}

function getUserById(userId, users) {
    return users.find(user => user.id === userId);
}


const users$ = fetch(' https://ajax.test-danit.com/api/json/users')
    .then(response => response.json());

const posts$ = fetch('   https://ajax.test-danit.com/api/json/posts')
    .then(response => response.json());


Promise.all([users$, posts$])
    .then(([users, posts]) => {
        const cards = posts.map((post) => {
            const user = getUserById(post.userId, users);
            return new Card(post.title, post.body, user.name, user.email , post.id);
        });
        const cardsHtml = cards.map(elem => {
            return createCardHtml(elem);
        });
        document.getElementById('root').innerHTML = cardsHtml.join('');
    });

function createCardHtml({title, body, name, email , postId}) {
    return `<div id='card-${postId}'><h2>${title}</h2> <p>${body}</p> <p>${name}</p> <a href='mailto:'>${email}</a> <button id="${postId}" class="button-delete">DELETE</button></div>`;
}

document.addEventListener("click", e=>{
    if (e.target.classList.contains('button-delete') === true){
        fetch(`https://ajax.test-danit.com/api/json/posts/${e.target.id}`, {
            method: 'delete'
        }) .then(response => {
           if (response.ok === true){
               document.getElementById(`card-${e.target.id}`).remove()
           }
        })
    }
})
