import React, {Component} from 'react';
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import './App.css';


class App extends Component {
    state = {
        showFirstModal: false,
        showSecondModal: false
    };

    handleFirstModal = () => {
        this.setState({showFirstModal: !this.state.showFirstModal});
    };

    handleSecondModal = () => {
        this.setState({showSecondModal: !this.state.showSecondModal});
    };

    render() {
        const {showFirstModal, showSecondModal} = this.state;
        return (
            <>
                {showFirstModal
                    ? <Modal header="Какойто заголовок"
                             text={<p>Что-то важное и нужное</p>}
                             actions={
                                 <>
                                     <button onClick={this.handleFirstModal}>Ok</button>
                                     <button onClick={this.handleFirstModal}>Cancel</button>
                                 </>
                             }
                             closeButton={this.handleFirstModal}/>
                    : <Button backgroundColor="green"
                              text="Open first modal"
                              onClick={this.handleFirstModal}/>
                }
                {showSecondModal
                    ? <Modal header="Какойто заголовок"
                             text={<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, quasi, soluta? Aspernatur earum facere fuga inventore maxime nesciunt odio tempore. Aliquid dicta doloremque ducimus harum, necessitatibus quam quia reiciendis tempore?</p>}
                             actions={
                                 <>
                                     <button onClick={this.handleSecondModal}>Заходи</button>
                                     <button onClick={this.handleSecondModal}>Выходи</button>
                                 </>
                             }
                             closeButton={this.handleSecondModal}/>
                    : <Button backgroundColor="red"
                              text="Open second modal"
                              onClick={this.handleSecondModal}/>
                }
            </>

        );
    }
}

export default App;
