import React, { Component } from "react";



class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (
            <div>
                <button style={{backgroundColor: backgroundColor }} onClick={onClick}>{text}</button>
            </div>
        )
    }
}
export default Button