import React, {Component} from 'react';
import './modal.css';


class Modal extends Component {
    render() {
        const { header, closeButton , actions, text} = this.props;
        return (
                    <div className="modal" onClick={closeButton}>
                        <div className="modal-dialog" >
                            <div className="modal-header">
                                <h3 className="modal-title">{header}</h3>
                                <span className="modal-close" onClick={closeButton}>
            &times;
          </span>
                            </div>
                            <div className="modal-body">
                                <div className="modal-content">{text}</div>
                            </div>
                            <div className="footer">
                                <div className="modal-footer">{actions}</div>
                            </div>
                        </div>
                    </div>
        );
    }
}


export default Modal;





