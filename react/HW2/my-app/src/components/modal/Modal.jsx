import React, {Component} from 'react';
import './modal.scss';


class Modal extends Component {
    render() {
        const {header, closeButton, actions, text} = this.props;
        return (
            <div className="modal d-block" onClick={closeButton} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{header}</h5>
                            <button type="button" className="btn-close" onClick={closeButton} data-bs-dismiss="modal"
                                    aria-label="Close"/>
                        </div>
                        <div className="modal-body">
                            {text}
                        </div>
                        <div className="modal-footer">
                            {actions}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default Modal;





