import React, {Component} from 'react';
import Button from "../button/Button";
import Modal from "../modal/Modal";
import PropTypes from 'prop-types';
import './card.scss';
import {StorageService} from "../../core/storageService";

class Card extends Component {
    state = {
        showFirstModal: false,
        // isFavorite: false
    };

    handleButtonFavorites = () => {
        this.setState({isFavorite: !this.state.isFavorite});

        const favorites = StorageService.getFavorites();
        if (favorites.includes(this.props.product.article) === true) {
            StorageService.deleteFavorite(this.props.product.article);
        } else {
            StorageService.addFavorite(this.props.product.article);
        }

        this.props.favorites();

    };


    handleModal = () => {
        this.setState({showFirstModal: !this.state.showFirstModal});
    };

    addToCard = () => {
        this.setState({showFirstModal: !this.state.showFirstModal});
        const carts = StorageService.getCart();
        if (carts.includes(this.props.product.article) === true) {
            StorageService.deleteFromCart(this.props.product.article);
        } else {
            StorageService.addToCart(this.props.product.article);
        }

        this.props.cart();

    };

    render() {
        const {showFirstModal} = this.state;
        const {name, price, url, article, color} = this.props.product;
        return (
            <div id={article} className="card">
                <div className="card-container">
                    <img src={url} alt=""/>
                    <button className="card-favorite" onClick={this.handleButtonFavorites}>
                        {this.props.isFavorite ?
                            <i className="bi bi-star-fill"/> : <i className="bi bi-star"/>}
                    </button>

                    <h2 className="card-title">{name}</h2>
                    <p className="card-color">Color:{color}</p>
                    <div className="footer">
                        <p className="price">{price}</p>
                        <Button backgroundColor="green"
                                text="Add to cart"
                                onClick={this.handleModal}/>
                        {showFirstModal
                        && <Modal header="Вы уверены что хотите добавить в корзину?"
                                  text={<p>{name}</p>}
                                  actions={
                                      <>
                                          <button type="button" className="btn btn-primary" onClick={this.addToCard}>Да
                                              уверен
                                          </button>
                                          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal"
                                                  onClick={this.handleModal}>Нет не уверен
                                          </button>
                                      </>
                                  }
                                  closeButton={this.handleModal}/>
                        }
                    </div>
                </div>
            </div>
        );
    }
}


Card.propTypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
};


export default Card;