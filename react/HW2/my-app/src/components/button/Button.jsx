import React, { Component } from "react";
import PropTypes from 'prop-types'


class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (
            <div>
                <button style={{backgroundColor: backgroundColor }} onClick={onClick}>{text}</button>
            </div>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
}

export default Button