import React, {Component} from 'react';
import PropTypes from 'prop-types'
import './header.scss';

class Header extends Component{

    render() {
        const{totalFavorites,totalCart} = this.props
        return (
            <div>
                <div>
                    <i className="bi bi-star-half"/>
                    Favorites
                    ({totalFavorites})
                </div>
                <div>
                    <i className="bi bi-cart4"/>
                    Cart
                    ({totalCart})
                </div>
            </div>
        );
    }
}


Header.propTypes = {
    totalCart: PropTypes.number,
    totalFavorites: PropTypes.number,
}

export default Header