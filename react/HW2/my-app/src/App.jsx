import React, {Component} from 'react';
import {getProduct} from "./api/Products";
import Card from "./components/cards/Card";
import Header from "./components/header/Header";
import {StorageService} from "./core/storageService";
import './App.css';

class App extends Component {
    state = {
        favorites: StorageService.getFavorites(),
        cart: StorageService.getCart(),
        products: [],
    };
    updateFavorites = () => {
        this.setState({favorites: StorageService.getFavorites()});
    };
    updateCart = () => {
        this.setState({cart: StorageService.getCart()});
    };

    componentDidMount() {
        getProduct()
            .then((products) => this.setState({products: [...this.state.products, ...products]}));
    }

    isFavorite = (article) => {
        return !!this.state.favorites.includes(article);

    };

    render() {
        return (
            <>
                <Header
                    totalFavorites={this.state.favorites.length}
                    totalCart={this.state.cart.length}/>
                {
                    this.state.products.map(product => {
                        return <Card favorites={this.updateFavorites} cart={this.updateCart} key={product.article}
                                     product={product} isFavorite={this.isFavorite(product.article)}/>;
                    })
                }
            </>
        );
    }
}

export default App;
