export class StorageService {

    static getCart() {
        return JSON.parse(localStorage.getItem('card')) || [];
    }

    static addToCart(article) {
        const cart = StorageService.getCart();
        const updatedCart = JSON.stringify([...cart, article]);
        localStorage.setItem('card', updatedCart);
    }

    static deleteFromCart(article) {
        const carts = StorageService.getCart();
        const valueIndex = carts.findIndex(item => item === article);

        const updatedArray = carts.filter((item, index) => index !== valueIndex);

        //
        // const updatedArray = carts.filter((value) => {
        //
        //     return value !== article && counter <= 1;
        // });

        return localStorage.setItem('card', JSON.stringify(updatedArray));
    }
    static clearCart(){
        localStorage.removeItem('card');

    }
    static getFavorites() {
        return JSON.parse(localStorage.getItem('favorite')) || [];
    }

    static addFavorite(article) {
        const favorite = StorageService.getFavorites();
        const updatedFavorites = JSON.stringify([...favorite, article]);
        localStorage.setItem('favorite', updatedFavorites);
    }

    static deleteFavorite(article) {
        const favorites = StorageService.getFavorites();

        const updatedArray = favorites.filter((value) => {
            return value !== article;
        });

        localStorage.setItem('favorite', JSON.stringify(updatedArray));
    }
}