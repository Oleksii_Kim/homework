import React from 'react';
import {StorageService} from "../../core/storageService";
import CartItem from "../../components/cartItem/CartItem";
import Submit from "../../components/Validation/validationCart";


const Cart = (props) => {

    if (props.products.length < 1) {
        return <h1>Your cart is empty.</h1>;
    }
    const deleteFromCart = (article) => {
        StorageService.deleteFromCart(article);
        props.cartUpdate();
    };

    const getProductByArticle = (article) => {
        return props.products.find((product) => product.article === article);
    };

    const getQuantity = (article) => {
        const cart = StorageService.getCart();
        return cart.filter((item) => {
            return item === article;
        }).length;
    };

    const newArray = Array.from(new Set(StorageService.getCart()));

    const productsWithQuantity = newArray.map(article => ({
            quantity: getQuantity(article),
            product: getProductByArticle(article)
        })
    );
    return (
        <>
            {
                props.products.map((product) => {
                    return (
                        <CartItem key={product.article} product={product}
                                  deleteFromCart={() => deleteFromCart(product.article)}
                                  quantity={getQuantity(product.article)}/>
                    );
                })
            }
            <Submit cartUpdate={props.cartUpdate} cartProducts={productsWithQuantity}/>
        </>
    );
};

export default Cart;