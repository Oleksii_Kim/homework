import React from 'react';
import Card from "../../components/cards/Card";
import './Home.scss'

const Home = (props) => {
    return (
        <div className='cards'>
            {
                props.products.map(product => {
                    return <Card isFavorite={props.isFavorite(product.article)} favorites={props.updateFavorites}
                              cartUpdate={props.updateCart} key={product.article}
                              product={product}/>;
                })
            }
        </div>
    );
};

export default Home;