import React from "react";
import PropTypes from 'prop-types'
import './Button.scss'

const Button = ({backgroundColor, text, onClick ,color,border,borderRadius})=> {
        return (
            <div className='button'>
                <button style={{backgroundColor: backgroundColor ,color:color , border:border,borderRadius:borderRadius}} onClick={onClick}>{text}</button>
            </div>
        )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
}

export default Button