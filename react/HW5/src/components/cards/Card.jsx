import React from 'react';
import Button from "../button/Button";
import Modal from "../modal/Modal";
import PropTypes from 'prop-types';
import './card.scss';
import {StorageService} from "../../core/storageService";
import {useDispatch, useSelector} from "react-redux";

const Card = (props) => {
    // const [showFirstModal, setShowFirstModal] = useState(false);
    const dispatch = useDispatch();
    const openedModalId = useSelector(state => state.openedModal);
    // const id = useSelector(state => state.id);
    const handleButtonFavorites = () => {

        const favorites = StorageService.getFavorites();
        if (favorites.includes(props.product.article) === true) {
            StorageService.deleteFavorite(props.product.article);
        } else {
            StorageService.addFavorite(props.product.article);
        }
        props.favorites();

    };


    const handleModal = (id) => {
        dispatch({type: 'TOGGLE_MODAL', payload: id});
        // setShowFirstModal(!showFirstModal);
    };

    const addToCard = () => {
        // setShowFirstModal({showFirstModal: !showFirstModal});
        dispatch({type: 'TOGGLE_MODAL', payload: null});
        StorageService.addToCart(props.product.article);
        props.cartUpdate();

    };
    return (
        <div id={props.product.article} className="card">
            <div className="card-container">
                <div className="card-image">
                    <img src={props.product.url} alt=""/>
                </div>
                <div className="card-content">
                    <div className="card-content__header">
                        <div className="card-content__header-info">
                            <h2 className="card-title">{props.product.name}</h2>
                            <p className="card-color">Color:{props.product.color}</p>
                        </div>

                        <button className="card-favorite" onClick={handleButtonFavorites}>
                            {props.isFavorite ?
                                <i className="bi bi-star-fill"/> : <i className="bi bi-star"/>}
                        </button>

                    </div>
                    <div className="card-content__footer">
                        <div className="price">Price: {props.product.price} grn.</div>
                        <Button
                            className='card-content__footer-button'
                            backgroundColor="blue"
                            color='white'
                            border='none'
                            borderRadius='7px'
                                text="Add to cart"
                                onClick={() => handleModal(props.product.article)}/>
                        {openedModalId === props.product.article
                        && <Modal header="Вы уверены что хотите добавить в корзину?"
                                  text={props.product.name}
                                  id={props.product.article}
                                  actions={
                                      <>
                                          <button type="button" className="btn btn-primary" onClick={addToCard}>Да
                                              уверен
                                          </button>
                                          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal"
                                                  onClick={() => handleModal(null)}>Нет не уверен
                                          </button>
                                      </>
                                  }
                                  closeButton={handleModal}/>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};


Card.propTypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
};


export default Card;