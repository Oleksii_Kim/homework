import Button from "../button/Button";
import Modal from "../modal/Modal";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import './CartItem.scss';

const CartItem = (props) => {
    // const [showModal, setShowModal] = useState(false);
    const dispatch = useDispatch();
    const openedModalId = useSelector(state => state.openedModal);
    const product = props.product;
    const handleModal = (id) => {
        dispatch({type: 'TOGGLE_MODAL', payload: id});

    };
    return (
        <div key={product.article} className="cart">
            <div className="cart-container">
                <div className="cart-left">
                    <img src={product.url} alt=""/>
                    <h3>{product.name}</h3>
                </div>
                <div className="cart__footer">
                    <p className="cart__footer-quantity">Количество:{props.quantity}</p>
                    <div className='cart__footer-bottom'>
                        <p className="cart__footer-price">Price: {product.price} grn.</p>
                        <Button
                            className="cart__footer-button"
                            backgroundColor="blue"
                            color="white"
                            border="none"
                            borderRadius="7px"
                            text="Delete"
                            onClick={() => handleModal(props.product.article)}/>
                        {openedModalId === props.product.article
                        && <Modal header="Вы уверены что хотите удалить елемент из корзины?"
                                  text={product.name}
                                  actions={
                                      <>
                                          <button type="button" className="btn btn-primary"
                                                  onClick={() => {
                                                      handleModal(null);
                                                      props.deleteFromCart(props.product.article);
                                                  }}>Да
                                              уверен
                                          </button>
                                          <button type="button" className="btn btn-secondary"
                                                  data-bs-dismiss="modal"
                                                  onClick={() => handleModal(null)}>Нет не уверен
                                          </button>
                                      </>
                                  }
                                  closeButton={handleModal}/>
                        }</div>

                </div>
            </div>
        </div>
    );
};
export default CartItem;

