import React from 'react';
import {StorageService} from "../../core/storageService";
import {Formik, Form, ErrorMessage} from 'formik';
import * as yup from 'yup';
import './validationCart.scss'

const CustomErrorMessage = ({children}) => {
    return (<div style={{color: 'red'}}><span>{children}</span></div>);
};

const validationSchema = yup.object().shape({
    firstName: yup.string().typeError('Должно быть строкой').min(4, 'Too Short!').max(12, 'Too Long!').required('Обязательно'),
    lastName: yup.string().typeError('Должно быть строкой').min(2, 'Too Short!').max(12, 'Too Long!').required('Обязательно'),
    age: yup.number().min(16, 'Incorrect age').max(100, 'Incorrect age!').required('Обязательно'),
    address: yup.string().min(5, 'Too Short!').max(30, 'Too Long!').required('Обязательно'),
    phone: yup.number().typeError('Must be number').required('Обязательно'),
});
const Submit = (props) => {

    const handleSubmit = (values) => {
        console.log(props.cartProducts);
        console.log(values.firstName,values.lastName,values.age,values.address,values.phone);
        StorageService.clearCart()
        props.cartUpdate()
    };

    return (
        <div className='validation'>
            <div className='validation-title'><h1>Confirm purchases</h1></div>
            <div className='validation-form'><Formik
                initialValues={{firstName: '', lastName: '', age: '', address: '', phone: ''}}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                {({values,
                      handleChange, handleBlur, isValid, handleSubmit, dirty}) => (
                    <Form>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`firstName`}>Name</label><br/>
                            <input type={"text"} name={`firstName`} onChange={handleChange} onBlur={handleBlur} value={values.firstName}/>
                            <ErrorMessage name="firstName" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`lastName`}>Last Name</label><br/>
                            <input type={"text"} name={`lastName`} onChange={handleChange} onBlur={handleBlur} value={values.lastName}/>
                            <ErrorMessage name="lastName" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`age`}>Age</label><br/>
                            <input type={"number"} name={`age`} onChange={handleChange} onBlur={handleBlur} value={values.age}/>
                            <ErrorMessage name="age" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`address`}>Address</label><br/>
                            <input type={"text"} name={`address`} onChange={handleChange} onBlur={handleBlur} value={values.address}/>
                            <ErrorMessage name="address" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`phone`}>Phone</label><br/>
                            <input type={"text"} name={`phone`} onChange={handleChange} onBlur={handleBlur} value={values.phone}/>
                            <ErrorMessage name="phone" component={CustomErrorMessage}/>
                        </div>

                        <div className='form-button'>
                            <button disabled={!isValid && !dirty} onClick={handleSubmit} type="submit">Checkout</button>
                        </div>

                    </Form>
                )}
            </Formik></div>

        </div>
    );
};

export default Submit;