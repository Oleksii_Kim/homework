import React from 'react';
import './modal.scss';


const Modal = (props) => {
   const {header, closeButton, actions, text} = props
    return (
        <div className="modal d-block" onClick={(e)=>{
            if (e.target.classList.contains('modal')){
                closeButton(null)
            }
        }} tabIndex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{header}</h5>
                        <button type="button" className="btn-close" onClick={()=>closeButton(null)} data-bs-dismiss="modal"
                                aria-label="Close"/>
                    </div>
                    <div className="modal-body">
                       <p> {text}</p>
                    </div>
                    <div className="modal-footer">
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    );
};


export default Modal;





