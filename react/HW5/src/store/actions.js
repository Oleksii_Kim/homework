import { MODAL_IS_OPEN } from "./types";

export const modalIsOpen = value => {
    return {
        type: MODAL_IS_OPEN,
        isOpen: value
    }
}
