import React, {useState, useEffect} from 'react';
import {Route, Routes} from 'react-router-dom';
import {getProduct} from "./api/Products";
import Header from "./components/header/Header";
import {StorageService} from "./core/storageService";
// import Card from "./components/cards/Card";
import './App.css';


import Home from "./pages/Home/Home";
import Favorites from "./pages/Favorites/Favorites";
import Cart from "./pages/Cart/Cart";

function App() {
    const [favorites, setFavorites] = useState(StorageService.getFavorites());
    const [cart, setCart] = useState(StorageService.getCart());
    const [products, setProducts] = useState([]);


    const getFavoriteProducts = () => {
        return products.filter((product) => {
                return isFavorite(product.article);
            }
        );
    };

    const getCartProducts = () => {
        return products.filter((product) => {
                return inCart(product.article);
            }
        );
    };

    const updateFavorites = () => {
        setFavorites(StorageService.getFavorites());
    };
    const updateCart = () => {
        setCart(StorageService.getCart());
    };
    // const deleteFromCart = () => {
    //     setCart(StorageService.getCart());
    // };
    const isFavorite = (article) => {
        return !!favorites.includes(article);
    };
    const inCart = (article)=>{
        return !!cart.includes(article);
    }


    useEffect(() => {
        getProduct()
            .then((product) => setProducts(products => [...products, ...product]));
    }, []);


    return (
        <>
            <Header
                totalFavorites={favorites.length}
                totalCart={cart.length}/>
            <Routes>
                <Route path="/"
                       element={<Home products={products} updateFavorites={updateFavorites} updateCart={updateCart}
                                      isFavorite={isFavorite}/>}/>
                <Route path="/favorites" element={<Favorites products={getFavoriteProducts()}/>}/>
                <Route path="/cart" element={<Cart cartUpdate={updateCart} products={getCartProducts()} />}/>
            </Routes>
        </>
    );
}

export default App;
