import React from 'react';
// import Button from "../../components/button/Button";
// import Modal from "../../components/modal/Modal";

const Favorites = (props) => {

    return (
        <>
            {
                props.products.map((product) => {

                    return (
                        <div key={product.article} className="card">
                            <div className="card-container">
                                <img src={product.url} alt=""/>
                                <h2 className="card-title">{product.name}</h2>
                                <p className="card-color">Color:{product.color}</p>
                                <div className="footer">
                                    <p className="price">{product.price}</p>
                                </div>
                            </div>
                        </div>
                    );
                })
            }
        </>
    );
};

export default Favorites;