import Button from "../button/Button";
import Modal from "../modal/Modal";
import React, {useState} from "react";

const CartItem = (props) => {
    const [showModal, setShowModal] = useState(false);
    const product = props.product;
    const handleModal = () => {
        setShowModal(!showModal);

    };
    return (
        <div key={product.article} className="card">
            <div className="card-container">
                <img src={product.url} alt=""/>

                <h2 className="card-title">{product.name}</h2>
                <p className="card-color">Color:{product.color}</p>
                <div className="footer">
                    <p className="price">{product.price}</p>
                    <Button backgroundColor="green"
                            text="Delete"
                            onClick={handleModal}/>
                    <p className="quantity">Количество:{props.quantity}</p>
                    {showModal
                    && <Modal header="Вы уверены что хотите удалить елемент из корзины?"
                              text={product.name}
                              actions={
                                  <>
                                      <button type="button" className="btn btn-primary"
                                              onClick={() => props.deleteFromCart(props.product.article)}>Да
                                          уверен
                                      </button>
                                      <button type="button" className="btn btn-secondary"
                                              data-bs-dismiss="modal"
                                              onClick={handleModal}>Нет не уверен
                                      </button>
                                  </>
                              }
                              closeButton={handleModal}/>
                    }
                </div>
            </div>
        </div>
    );
};
export default CartItem
