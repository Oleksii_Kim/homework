import React, {useState} from 'react';
import Button from "../button/Button";
import Modal from "../modal/Modal";
import PropTypes from 'prop-types';
import './card.scss';
import {StorageService} from "../../core/storageService";

const Card = (props) => {
    const [showFirstModal, setShowFirstModal] = useState(false);

    const handleButtonFavorites = () => {

        const favorites = StorageService.getFavorites();
        if (favorites.includes(props.product.article) === true) {
            StorageService.deleteFavorite(props.product.article);
        } else {
            StorageService.addFavorite(props.product.article);
        }
        props.favorites();

    };


    const handleModal = () => {
        setShowFirstModal(!showFirstModal);
    };

    const addToCard = () => {
        setShowFirstModal({showFirstModal: !showFirstModal});
        StorageService.addToCart(props.product.article);
        props.cartUpdate();

    };
    return (
        <div id={props.product.article} className="card">
            <div className="card-container">
                <img src={props.product.url} alt=""/>
                <button className="card-favorite" onClick={handleButtonFavorites}>
                    {props.isFavorite ?
                        <i className="bi bi-star-fill"/> : <i className="bi bi-star"/>}
                </button>


                <h2 className="card-title">{props.product.name}</h2>
                <p className="card-color">Color:{props.product.color}</p>
                <div className="footer">
                    <p className="price">{props.product.price}</p>
                    <Button backgroundColor="green"
                            text="Add to cart"
                            onClick={handleModal}/>
                    {showFirstModal
                    && <Modal header="Вы уверены что хотите добавить в корзину?"
                              text={props.product.name}
                              actions={
                                  <>
                                      <button type="button" className="btn btn-primary" onClick={addToCard}>Да
                                          уверен
                                      </button>
                                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal"
                                              onClick={handleModal}>Нет не уверен
                                      </button>
                                  </>
                              }
                              closeButton={handleModal}/>
                    }
                </div>
            </div>
        </div>
    );
};


Card.propTypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
};


export default Card;