import React from 'react';
import Card from "../../components/cards/Card";

const Home = (props) => {
    return (
        <>
            <div>Home</div>
            {
                props.products.map(product => {
                    return <Card isFavorite={props.isFavorite(product.article)} favorites={props.updateFavorites} cartUpdate={props.updateCart} key={product.article}
                                 product={product}/>;
                })
            }
        </>
    )
}

export default Home