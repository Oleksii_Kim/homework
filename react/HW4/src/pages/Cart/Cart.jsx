import React from 'react';
import {StorageService} from "../../core/storageService";
import CartItem from "../../components/cartItem/CartItem";



const Cart = (props) => {
    const deleteFromCart = (article) => {
        StorageService.deleteFromCart(article);
        props.cartUpdate();
    };

    const getQuantity = (article) => {
        const cart = StorageService.getCart();
        return cart.filter((item) => {
            return item === article;
        }).length;
    };

    return (
        <>
            {
                props.products.map((product) => {
                    return (
                       <CartItem key={product.article} product={product} deleteFromCart={()=>deleteFromCart(product.article)} quantity={getQuantity(product.article)} />
                    );
                })
            }
        </>
    );
};

export default Cart;