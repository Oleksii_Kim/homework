import Button from "../button/Button";
import Modal from "../modal/Modal";
import React from "react";
import {useDispatch, useSelector} from "react-redux";

const CartItem = (props) => {
    // const [showModal, setShowModal] = useState(false);
    const dispatch = useDispatch();
    const openedModalId = useSelector(state => state.openedModal);
    const product = props.product;
    const handleModal = (id) => {
        dispatch({type: 'TOGGLE_MODAL', payload: id});

    };
    return (
        <div key={product.article} className="card">
            <div className="card-container">
                <img src={product.url} alt=""/>

                <h2 className="card-title">{product.name}</h2>
                <p className="card-color">Color:{product.color}</p>
                <div className="footer">
                    <p className="price">{product.price}</p>
                    <Button backgroundColor="green"
                            text="Delete"
                            onClick={() => handleModal(props.product.article)}/>
                    <p className="quantity">Количество:{props.quantity}</p>
                    {openedModalId === props.product.article
                    && <Modal header="Вы уверены что хотите удалить елемент из корзины?"
                              text={product.name}
                              actions={
                                  <>
                                      <button type="button" className="btn btn-primary"
                                              onClick={() => {
                                                  handleModal(null);
                                                  props.deleteFromCart(props.product.article);
                                              }}>Да
                                          уверен
                                      </button>
                                      <button type="button" className="btn btn-secondary"
                                              data-bs-dismiss="modal"
                                              onClick={() => handleModal(null)}>Нет не уверен
                                      </button>
                                  </>
                              }
                              closeButton={handleModal}/>
                    }
                </div>
            </div>
        </div>
    );
};
export default CartItem;
