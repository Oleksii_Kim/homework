import React from "react";
import PropTypes from 'prop-types'


const Button = ({backgroundColor, text, onClick})=> {
        return (
            <div>
                <button style={{backgroundColor: backgroundColor }} onClick={onClick}>{text}</button>
            </div>
        )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
}

export default Button