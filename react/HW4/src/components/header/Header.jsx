import React from 'react';
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom';
// import {useSelector} from "react-redux";
import './header.scss';

const Header =({totalFavorites, totalCart}) => {
        return (
            <div>
                <div>
                    <NavLink style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/">Home</NavLink>
                </div>
                <div>
                    <i className="bi bi-star-half"/>
                    <NavLink style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/favorites">Favorites</NavLink>
                    ({totalFavorites})
                </div>
                <div>
                    <i className="bi bi-cart4"/>
                    <NavLink style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/cart">Cart</NavLink>
                    ({totalCart})
                </div>
            </div>
        );
}


Header.propTypes = {
    totalCart: PropTypes.number,
    totalFavorites: PropTypes.number,
}

export default Header