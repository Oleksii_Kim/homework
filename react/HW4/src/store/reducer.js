const reducer = (state, action) => {
    switch (action.type) {
        case 'SET_PRODUCTS': {
            return {...state, products: action.payload};
        }
        case 'TOGGLE_MODAL': {
            return {...state, openedModal: action.payload};
        }
        default: {
            return state;
        }
    }
};

export default reducer;