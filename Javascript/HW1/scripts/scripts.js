// ## Теоретический вопрос
//
// 1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// 2. Почему объявлять переменную через var считается плохим тоном?
//
//     ## Задание
//
// Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с помощью модальных окон браузера - `alert`, `prompt`, `confirm`. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// - Если возраст меньше 18 лет - показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: `Are you sure you want to continue?`
// и кнопками `Ok`, `Cancel`. Если пользователь нажал `Ok`, показать на экране сообщение: `Welcome, ` + имя пользователя.
// Если пользователь нажал `Cancel`, показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст больше 22 лет - показать на экране сообщение: `Welcome, ` + имя пользователя.
// - Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
//
//     #### Необязательное задание продвинутой сложности:
//     - После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число -
//     спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
//
// #### Литература:
// - [Взаимодействие с пользователем: alert, prompt, confirm](https://learn.javascript.ru/uibasic)
// - [Переменные: let и const](https://learn.javascript.ru/let-const)

// TODO теория
// todo
//  1.
//  const - переменная которую мы не можем изменять, переопредилять в дальнейшем
//  let - переменная которую мы можем изменять, переопредилять в дальнейшем
//  var - область видимости не блочная а фукциональная
//  2.
//  Изза того что в фукциональной области видимости может к примеру находиться несколько блоков, в котором мы например сможем нечайно переопределили переменную. Это как один вариантов почему не стоит обьявлять переменную через вар а стараться делать все через конст




// TODO Практика
let userAge;
let userName = prompt('Введите ваше имя!');
if (userName === null) {
    alert('You are not allowed to visit this website');
} else if (userName === '') {
    userName = prompt('Введите ваше имя , заново!');
    // todo  здесь мы представляем что во второй раз пользователь вводит имя правильно !
    if (userName) {
        userAge = prompt('Введите ваш возраст!');
        if (userAge === null) {
            alert('You are not allowed to visit this website');
        } else {
            userAge = Number(userAge);
            if (Number.isNaN(userAge)) {
                userAge = +prompt('Введите повторно возраст!');
                // todo  здесь мы представляем что во второй раз пользователь вводит год правильно !
            }
            if (userAge < 18) {
                alert('You are not allowed to visit this website');
            } else if (userAge >= 18 && userAge <= 22) {
                const userAgreement = confirm('Are you sure you want to continue?');
                if (userAgreement) {
                    alert(`Welcome, ${userName}`);
                } else {
                    alert(`You are not allowed to visit this website`);
                }
            } else {
                alert(`Welcome, ${userName}`);
            }
        }
    }
} else {
    userAge = prompt('Введите ваш возраст!');
    if (userAge === null) {
        alert('You are not allowed to visit this website');
    } else {
        userAge = Number(userAge);
        if (Number.isNaN(userAge) || userAge === '') {
            userAge = +prompt('Введите повторно возраст!');
            if (userAge < 18) {
                alert('You are not allowed to visit this website');
            } else if (userAge >= 18 && userAge <= 22) {
                const userAgreement = confirm('Are you sure you want to continue?');
                if (userAgreement) {
                    alert(`Welcome, ${userName}`);
                } else {
                    alert(`You are not allowed to visit this website`);
                }
            } else {
                alert(`Welcome, ${userName}`);
            }
        }
    }
    if (userAge < 18) {
        alert('You are not allowed to visit this website');
    } else if (userAge >= 18 && userAge <= 22) {
        const userAgreement = confirm('Are you sure you want to continue?');
        if (userAgreement) {
            alert(`Welcome, ${userName}`);
        } else {
            alert(`You are not allowed to visit this website`);
        }
    } else {
        alert(`Welcome, ${userName}`);
    }
}
