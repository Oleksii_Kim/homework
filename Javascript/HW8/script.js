// ## Теоретический вопрос
//
// 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//
//     ## Задание
//
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. Это поле будет служить для ввода числовых значений
// - Поведение поля должно быть следующим:
//     - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// - Когда убран фокус с поля - его значение считывается, над полем создается `span`,
// в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`. Рядом с ним должна быть кнопка с крестиком (`X`). Значение внутри поля ввода окрашивается в зеленый цвет.
// - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
// под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
// - В папке `img` лежат примеры реализации поля ввода и создающегося `span`.
//
//     #### Литература:
// - [Поиск DOM элементов](https://learn.javascript.ru/searching-elements-dom)
// - [Добавление и удаление узлов](https://learn.javascript.ru/modifying-document)
// - [Введение в браузерные события](https://learn.javascript.ru/introduction-browser-events)

// теория
// Обработчик событий это функция котроя будет работать как только произошло какоэто событие, в этом случае он может реаагироватаь на действия пользователя


//практика
const inputPrice = document.getElementById('input');
inputPrice.addEventListener('focus', () => {
    inputPrice.style.border = '3px solid green';
});
inputPrice.addEventListener('blur', event => {
    inputPrice.style.border = '';
    const errorNumber = document.getElementById('error')
    if (inputPrice.value <= 0){
        inputPrice.style.color = 'red'
        inputPrice.style.border = '3px solid red'
        errorNumber.innerHTML = 'Please enter correct price'
    } else {
        const chip = createChips(event.target.value);
        addChips(chip)
        inputPrice.style.color = 'green'
    }
});

function createChips(price) {
    const spanElement = document.createElement("span");
    spanElement.innerHTML = `Текущая цена: ${price}`;
    const buttonRemove = document.createElement("button");
    buttonRemove.innerHTML = 'X';
    buttonRemove.addEventListener('click' , () =>{
        spanElement.remove()
        inputPrice.value = ''
    })
    spanElement.append(buttonRemove);
    return spanElement
}

function addChips(chips){
    const chipsCont = document.getElementById('chips_container')
    chipsCont.append(chips)
}
