// ## Теоретический вопрос
//
// 1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//
// ## Задание
//
// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//     1. При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
// 2. Создать метод `getAge()` который будет возвращать сколько пользователю лет.
// 3. Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре,
// соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.
//
//     #### Литература:
// - [Дата и время](https://learn.javascript.ru/datetime)
//Теория
// экранирование используем что бы переобразовать наши символы на соответствующие текстовые посдстановки.
// в програмирование их зачастую используют для того что бы обезопасить то что вводит пользователь
// (можно в кратце скаазать что это защита от хакеров)

function createNewUser() {
    const newUser = {
        firstname : setFirstName(),
        lastName : setLastName(),
        birthday: getBirthday(),
       getAge: function (){
            return new Date().getFullYear() - newUser.birthday.split('.')[2]
       },
        getPassword: function (){
            return newUser.firstname.charAt(0).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.split('.')[2]
        }
    }
    console.log(newUser.getAge())
    console.log(newUser.getPassword());
    return {
        newUser
    };
}
function getBirthday(){
    let birthday = prompt('please type your birthday');
    while (!isValidString(birthday)) {
        birthday = prompt('please type your birthday');
    }
    return birthday;
}

function setFirstName() {
    let name = prompt('please type your name');
    while (!isValidString(name)) {
        name = prompt('please type your name');
    }
    return name;
}

function setLastName() {
    let lastName = prompt('please type your lastname');
    while (!isValidString(lastName)) {
        lastName = prompt('please type your lastname');
    }
    return lastName;
}

function isValidString(value) {
    return isNotEmptyString(value);
}
function isNotEmptyString(string) {
    return string !== '' && string !== null;
}

createNewUser()
