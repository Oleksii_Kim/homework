// ## Теоретический вопрос
//
// 1. Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
//
//     ## Задание
//
// Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - Считать с помощью модального окна браузера число, которое введет пользователь.
// - Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// - Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
//
// #### Необязательное задание продвинутой сложности:
// - Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
// - Считать два числа, `m` и `n`. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
// в диапазоне от `m` до `n` (меньшее из введенных чисел будет `m`, бОльшее будет `n`). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше,
// вывести сообщение об ошибке, и спросить оба числа заново.
//
// #### Литература:
// - [Циклы while, for](https://learn.javascript.ru/while-for)

// Теория
// если вкратце циклы используються для того что б сделать повторения и выполнения той или иной команды или кода енное количество раз (запускает цикл)


//Практика


// простая сложность
// let userNumber = +prompt('Please type your number!')
//
// for (let i = 0 ; i <= userNumber ; i++){
//     const remain = i % 5
//     if (!remain){
//         console.log(i);
//     } else if (userNumber < 5){
//         alert('Sorry, no numbers!')
//     }
// }


// продвинутая сложность
let m = prompt('please type first number');
// console.log(!Number.isInteger(m) || m === '' || Number.isNaN(+m) || m === null);
while (!Number.isInteger(+m) || m === '' || Number.isNaN(+m) || m === null) {
    m = +prompt('please type first number');
}

let n = prompt('please type second number');
while (!Number.isInteger(+n) || n === '' || Number.isNaN(+n) || n === null) {
    n = +prompt('please type second number');
}
if (m > n) {
    alert('Error!');
    let m = prompt('please type first number');
// console.log(!Number.isInteger(m) || m === '' || Number.isNaN(+m) || m === null);
    while (!Number.isInteger(+m) || m === '' || Number.isNaN(+m) || m === null) {
        m = +prompt('please type first number');
    }

    let n = prompt('please type second number');
    while (!Number.isInteger(+n) || n === '' || Number.isNaN(+n) || n === null) {
        n = +prompt('please type second number');
    }
    for (let i = m; i < n; i++) {
        let counter = 0;
        for (let j = 1; j <= i; j++) {
            if ((i % j) === 0) {
                counter = counter + 1;
            }
        }
        if (counter <= 2) {
            console.log(i);
        }
    }
}
for (let i = m; i < n; i++) {
    let counter = 0;
    for (let j = 1; j <= i; j++) {
        if ((i % j) === 0) {
            counter = counter + 1;
        }
    }
    if (counter <= 2) {
        console.log(i);
    }
}


