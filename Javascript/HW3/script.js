// ## Теоретический вопрос
//
// 1. Описать своими словами для чего вообще нужны функции в программировании.
// 2. Описать своими словами, зачем в функцию передавать аргумент.
//
//     ## Задание
//
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.
//
//     #### Необязательное задание продвинутой сложности:
//     - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа,
//     - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
//
// #### Литература:
// - [Функции - основы](https://learn.javascript.ru/function-basics)

//теория
// функции в програмирование нудъжны для того что б облегчить наш код к примеру что б мы смогли разбить свою задачу на под задачи и тем самым использовать уже готовые функции тоесть эти подзадачи в дальнейшем
// функцию мы передаем аргумент что б потом в дальнейшем использовать раные данные при данной функцие и изменять их тоесть вводить енное количество раз разные данные и получать результат

let number1 = prompt('please type first number!');
while (!isValidNumber(number1)) {
    number1 = prompt('please type first number!');
}

let number2 = prompt('please type second number!');
while (!isValidNumber(number2)) {
    number2 = prompt('please type second number!');
}
let mathOperation = prompt('please type your mathematics operation');

function calcNumber(firstNumber, secondNumber, mathOperation) {
    if (mathOperation === '+') {
        return +firstNumber + +secondNumber;
    } else if (mathOperation === '-') {
        return +firstNumber - +secondNumber;
    } else if (mathOperation === '*') {
        return +firstNumber * +secondNumber;
    } else if (mathOperation === '/') {
        return +firstNumber / +secondNumber;
    }
}

function isValidNumber(value) {
    return isNotEmptyNumber(value);
}

function isNotEmptyString(string) {
    return string !== '' && string !== null;
}

function isNotEmptyNumber(value) {
    return isNotEmptyString(value) && !Number.isNaN(+value);
}

console.log(calcNumber(number1, number2, mathOperation));
