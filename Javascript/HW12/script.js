// #Теоретический вопрос
//
// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
//
//     ## Задание
//
// Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// #### Технические требования:
//     - В папке `banners` лежит HTML код и папка с картинками.
// - При запуске программы на экране должна отображаться первая картинка.
// - Через 3 секунды вместо нее должна быть показана вторая картинка.
// - Еще через 3 секунды - третья.
// - Еще через 3 секунды - четвертая.
// - После того, как покажутся все картинки - этот цикл должен начаться заново.
// - При запуске программы где-то на экране должна появиться кнопка с надписью `Прекратить`.
// - По нажатию на кнопку `Прекратить` цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// - Рядом с кнопкой `Прекратить` должна быть кнопка `Возобновить показ`, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//
//     #### Необязательное задание продвинутой сложности:
//     - При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
// - Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.
//
//     #### Литература:
// - [setTimeout и setInterval](https://learn.javascript.ru/settimeout-setinterval)


//теория
//1.разница в том что setTimeout вызывает функцию один раз через определнное время а setInterval позволяет вызывать функцию регулярно через определенный интервал времени
//2. она сработает не мгновенно но настолько быстро насколько это возможно после завершенния основного кода,
//3. что бы в дальнейшем в коде эта функция не вессила и не потребляла мощности  так как если мы не пропишем clearInterval он будет выполняться и дальше

function startProgram(){
    createButtonStart()
    createButtonStop()

}

let interval =  window.setInterval(showImg,3000)
const imageArray = ['1.jpg','2.jpg','3.JPG','4.png']
const slider = document.querySelector('#slider')
const images = slider.querySelector('img')
const sliderContainer = document.getElementById('slider')
let i = 1
images.src ='./img/' + imageArray[0]

function showImg(){
    images.src ='./img/' + imageArray[i]
    i++
    if (i === imageArray.length){
        i = 0
    }
}

function createButtonStart(){
    const buttonStart =  document.createElement('button')
    buttonStart.innerHTML = 'Возобновить показ'
    sliderContainer.append(buttonStart)
    buttonStart.addEventListener('click',event=>{
        interval =  window.setInterval(showImg,3000)
    })
}

function createButtonStop(){
    const buttonStop =   document.createElement('button')
    buttonStop.innerHTML = 'Прекратить'
    sliderContainer.append(buttonStop)
    buttonStop.addEventListener('click',event=>{
        clearInterval(interval);
    })
}

startProgram()