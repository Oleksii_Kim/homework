/*
## Теоретический вопрос

1. Опишите своими словами, что такое метод обьекта

## Задание

Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
    - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
- При вызове функция должна спросить у вызывающего имя и фамилию.
- Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
- Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя,
соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
- Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

    #### Необязательное задание продвинутой сложности:
    - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.

    #### Литература:
- [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
- [Object.defineProperty()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)
*/
// теория
// ОБЬЕКТЫ, К ПРИМЕРУ У НАС ЕСТЬ ПОЛЬЗОВАТЕЛЬ ТО В СЛУЧАЕ РАБОТЫ С ЕГО ДАННЫМИ ПРОЩЕ БУДЕТ ВНЕСТИ ИХ В ОБЬЕКТ И ПОСЛЕ МЫ МОЖЕМ ОБРАЩАТЬСЯ К ЕГО ДАННЫМ ЧЕРЕЗ НЕЙМИНГ СОЗДАНОГО  ОБЬЕКТА.
//ТОЕСТЬ ПРИ ПОМОЩИ МЕТОДА ОБЬЕКТА МЫ МОЖЕМ ИМЕТЬ ДОСТУП К ИНФОРМАЦИИ КОТОРАЯ НАХОДИТЬСЯ В ЭТОМ ОБЬЕКТЕ И ПОТОМ К ЭТУ ЖЕ ИНФОРМАЦИЮ ИСПОЛЬЗОВАТЬ В ДАЛЬНЕЙШЕМ

// практика

// function startProgram() {
//     const newUser = createNewUser(
//         getName(),
//         getLastName(),
//         );
//     console.log(getLogin(newUser.name, newUser.lastName))
// }

function createNewUser() {
    const newUser = {
        firstname : setFirstName(),
        lastName : setLastName(),
        getLogin: function () {
            return newUser.firstname.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
        }
    }
    console.log(newUser.getLogin());
    return {
        newUser
    };
}

function setFirstName() {
    let name = prompt('please type your name');
    while (!isValidString(name)) {
        name = prompt('please type your name');
    }
    return name;
}

function setLastName() {
    let lastName = prompt('please type your lastname');
    while (!isValidString(lastName)) {
        lastName = prompt('please type your lastname');
    }
    return lastName;
}

function isValidString(value) {
    return isNotEmptyString(value);
}

function isNotEmptyString(string) {
    return string !== '' && string !== null;
}

createNewUser()
